const base64 = require('base-64');
const axios = require('axios');
var parseString = require('xml2js').parseString;

function requestApi(payload, options) {
  return new Promise((resolve, reject) => {
    var opt = {
      headers: {
        "Authorization": "Basic " + base64.encode(payload.iparams.apiKey + ":x"),
        "Content-Type": "text/xml; charset=utf-8"
      }
    };
    if ('body' in options) {
      opt['body'] = options.body;
    }
    var url = options.url;
    $request[options.method](url, opt).then(function (data) {
      resolve(data);
    }, function (error) {
      reject(error);
    });
  });
};

exports = {
response: function(data){
  var options = {
    url: data.iparams.domain,
    method: 'post',
    body: '<?xml version="1.0" encoding="utf-8"?><request method="currency.list"></request>',
  }
  requestApi(data, options).then(function (data) {
    var xml = data.response
    parseString(xml, function (err, result) {
      renderData(null,result.response)
      console.log(result.response.currencies[0].currency[0]);
    });
  }).catch(function (error) {
    renderData(null,error)
  })
},
getFDResponse: function(options){
  const base_url = options.iparams.domainName.startsWith('https://') ? options.iparams.domainName : 'https://' + options.iparams.domainName;
  const requrl = `${base_url}/${options.url}`;
  var opt = {
    headers: {
      "Authorization": "Basic <%= encode(iparam.apiKey) %>",
      "Content-Type": "application/json; charset=utf-8"
    }
  };
  $request.get(requrl, opt).then(function(data) {
    console.log(data)
    renderData(null,  { "data": JSON.parse(data.response) });
  }, function(err){
    renderData(null, { "error": err });
  });
}
}
