function FreshbooksAPIHelper(client) {
  this.client = client;
  this.descArr = [];
}

FreshbooksAPIHelper.prototype.getFBResponse = function (options) {
  const client = this.client;
  const url = options.url;
  const method = options.method;
  var opt = {
    headers: {
      "Authorization": "Bearer <%= access_token %>",
      "Api-Version": "alpha"
    },
    isOAuth: true
  };
  if ('body' in options) {
    opt['body'] = JSON.stringify(options.body);
  }
  return new Promise(function (resolve, reject) {
    client.request[method](url, opt).then(function(data){
      if (data && data.response) {
        resolve(JSON.parse(data.response));
      } else {
        resolve();
      }
    }, function(err){
      reject(err);
    });
  });
}