$(document).ready(function () {
  app.initialized().then(function (_client) {
    window.client = _client;
    // var client = _client;
    client.events.on('app.activated', function () {
      appActivate()
      console.clear()
    })
  });
});

this.appActivate = function(){
  getRequester();
}

function getRequester(){
  Promise.all([client.iparams.get(), client.data.get("requester")]).then(function ([iparamsData, reqData]) {
    console.log("reqData :",reqData);
    this.email_id = reqData.requester.email;
    // this.invoiceForAgents = iparamsData.invoiceForAgents;
    // this.timeEntryForAgents = iparamsData.timeEntryForAgents;
    // this.showCmcData = iparamsData.showCmcData;
    getRequesterCompany(reqData.requester.company_id);
  }).catch(function (error) {
    notify(error);
  });
}

function getRequesterCompany(reqCmpnyId){
  if (reqCmpnyId) {
    const options = {
      url: `api/v2/companies/${reqCmpnyId}`
    };
    client.request.invoke("getFDResponse", options).then(function (data) {
      const cmpnydata = data.response.data;
      this.reqCmpnyName = cmpnydata.name;
      getAccountDetails();
    }).catch(function (error) {
      console.log("Error :",error);
      notify(error);
    });
  } else {
    getAccountDetails();
  }
};

this.notify = function (error) {
  client.interface.trigger("showNotify", {
    type: "danger",
    message: error.message
  });
};
